import { SIGN_UP, LOG_IN } from './actions';


const API = 'http://localhost:3000/api/v1';

export const apiMiddleware = store => next => action => {
  switch (action.type) {
    case SIGN_UP:
      fetch(`${API}/signup`, {
        method: 'POST',
        headers: {
          'Accept': 'application/json',
          'Content-Type': 'application/json'
        },
        body: JSON.stringify(action.userData)
      })
      .then((res) => {
        console.log("AAAAAA-STATUS"+res.status+"STATUS-AAAAA")
        next({
          type: 'SIGN_UP_SUCCESSFUL'
        })
      })
      .catch((err) => {
        console.log("******");
        console.log(err);
        console.log("******");
      });
      break;
    case LOG_IN:
      fetch(`${API}/login`, {
        method: 'POST',
        headers: {
          'Accept': 'application/json',
          'Content-Type': 'application/json'
        },
        body: JSON.stringify(action.userData)
      })
      .then(res => res.json())
      .then((data) => {
        // console.log("AAAAAA-STATUS"+res.status+"STATUS-AAAAA")
        // if (res.status === 401 || res.status === 404) {
        // console.log("**********"+res.status+"**********")
        // }
        console.log("IN SERVER")
        console.log(data)
        console.log("IN SERVER")
        next({
          type: 'LOG_IN_SUCCESSFUL',
          data
        })
      })
      .catch((err) => {
        console.log("******");
        console.log(err);
        console.log("******");
      });
      break;
    case 'UPDATE_STATE_FROM_API':
      return
    default:
      break;
  }
}

export const reducer = (state = {}, action ) => {
  switch (action.type) {
    case 'SIGN_UP_SUCCESSFUL':
      console.log("IN REDUCER")
      return {
        ...state,
        signUpSuccessful: true
      }
      break;
    case 'LOG_IN_SUCCESSFUL':
      console.log("IN REDUCER")
      return {
        username: action.data.username,
        email: action.data.email,
        password: action.data.password
      }
    default:
      return state
  }

}
