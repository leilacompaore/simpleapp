const API = 'http://localhost:3000/api/v1';


export const apiMiddleware = store => next => action => {
  next(action);
  switch(action.type) {
    case 'CREATE_USER':
      // store.dispatch({type: 'CREATE_USER_LOADING'});
      fetch(`${API}/user`, {
        method: 'POST',
        headers: {
          'Accept': 'application/json',
          'Content-Type': 'application/json'
        },
        body: action.userData
      })
      .then((res) => console.log("AAAAAA-STATUS"+res.status+"STATUS-AAAAA"))
      .catch((err) => {
        console.log("******");
        console.log(err);
        console.log("******");
      });
      break;
    default:
      break;

  }
};

export const reducer = (state = {}, action) => {
  switch(action.type) {
    case 'CREATE_USER':
      fetch(`${API}/user`, {
        method: 'POST',
        headers: {
          'Accept': 'application/json',
          'Content-Type': 'application/json'
        },
        body: JSON.stringify(action.userData)
      })
      .then((res) => console.log("AAAAAA-STATUS"+res.status+"STATUS-AAAAA"))
      .catch((err) => {
        console.log("******");
        console.log(err);
        console.log("******");
      });
      return state;
    case 'LOG_IN':
      fetch(`${API}/login`, {
        method: 'POST',
        headers: {
          'Accept': 'application/json',
          'Content-Type': 'application/json'
        },
        body: JSON.stringify(action.userData)
      })
      .then((res) => {
        console.log("AAAAAA-STATUS"+res.status+"STATUS-AAAAA")
        if (res.status === 200) {

        }
      })
      .catch((err) => {
        console.log("******");
        console.log(err);
        console.log("******");
      });
      return state;
    default:
      return state;
  }
};
