//ACTIONs types
export const SIGN_UP = 'SIGN_UP'
export const LOG_IN = 'LOG_IN'

//ACTIONs creators
export const signup = (data) => {
  return {
    type: SIGN_UP,
    userData: data
  }
}

export const login = (data) => {
  return {
    type: LOG_IN,
    userData: data
  }
}
