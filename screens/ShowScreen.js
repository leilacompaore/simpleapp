import React from 'react';
import { StyleSheet, View, Text } from 'react-native';
import { AppLoading, Asset, Font } from 'expo';
import { Button } from 'react-native-elements';

export default class ShowScreen extends React.Component {

  render() {
    const nav = this.props.navigation;
    return (
        <View style={styles.container}>
          <View style={styles.inView}>
            <Button
              title='SIGN UP'
              onPress={()=>nav.navigate('Sign')}
              />
            <Button
              title='LOG IN'
              onPress={()=>nav.navigate('Log')}
              />
          </View>
        </View>
      );
    }
  }

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#fff',
  },
  inView : {
    marginTop: 200
  }
});
