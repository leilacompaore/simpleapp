import React from 'react';
import { connect } from 'react-redux';
import { StyleSheet, View, Text, Alert } from 'react-native';
import { FormLabel, FormInput, Button } from 'react-native-elements';

import { signup } from '../src/actions';
import { apiMiddleware, reducer, createUser } from '../src/reducers';

class SignScreen extends React.Component {
  constructor(props){
    super(props)
  }
  state = {
    username: '',
    email: '',
    password: ''
  }
  render() {
    return(
      <View style={styles.inView}>
        <Text>SIGN IN</Text>
        <FormLabel>Username</FormLabel>
        <FormInput onChangeText={(text)=>this.setState({username: text})}/>
        <FormLabel>Email</FormLabel>
        <FormInput onChangeText={(text)=>this.setState({email: text})}/>
        <FormLabel>Password</FormLabel>
        <FormInput onChangeText={(text)=>this.setState({password: text})}/>
        <Button
        title='GO'
        onPress={()=> {
          if(!this.state.username || !this.state.email || !this.state.password){
            Alert.alert('Fill in everything')
          } else {
          // this.props.dispatch(createUser(this.state));
          console.log('leila')
          this.props.signupGo(this.state);
          this.setState({signUpSuccessful:this.props.result.signUpSuccessful})
          this.props.navigation.navigate('Test', this.state)
          }
        }}
        />
      </View>
    );
  }
}


const styles = StyleSheet.create({
  inView : {
    marginTop: 50
  }
});

const mapDispatchToProps = dispatch => {
  return {
    signupGo: data => {
      dispatch(signup(data))
    }
  }
}

const mapStateToProps = state => {
  return {
    result: state
  }
}

export default connect (mapStateToProps, mapDispatchToProps) (SignScreen)
