import React from 'react';
import { StyleSheet, View, Text } from 'react-native';

export default class TestScreen extends React.Component {

  constructor(props){
    super(props)
  }

  render() {
    const {state} = this.props.navigation; //check Destructuring assignment and this.props.navigation.state
    return (
        <View style={styles.container}>
          <Text>Test</Text>
          <Text>{state.params.username}</Text>
          <Text>{state.params.email}</Text>
          <Text>{state.params.password}</Text>
          <Text>{state.params.signUpSuccessful}</Text>
        </View>
      );
    }
  }

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#fff',
    marginTop: 200
  }
});
