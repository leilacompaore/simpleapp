import React from 'react';
import { StyleSheet, View, Text } from 'react-native';

export default class App extends React.Component {

  render() {
    return (
        <View style={styles.container}>
          <Text>hey</Text>
        </View>
      );
    }
  }

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#fff',
    marginTop: 200
  }
});
