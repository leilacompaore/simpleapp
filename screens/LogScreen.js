import React from 'react';
import { StyleSheet, View, Text } from 'react-native';
import { FormLabel, FormInput, Button } from 'react-native-elements'
import { connect } from 'react-redux'
import { login } from '../src/actions';
import { apiMiddleware, reducer } from '../src/reducers';


class LogScreen extends React.Component {
  constructor(props){
    super(props)
  }
  state = {
    username: '',
    password: ''
  }
  render() {
    return (
        <View style={styles.inView}>
          <Text>LOG</Text>
          <FormLabel>Username</FormLabel>
          <FormInput
            onChangeText={(text) => this.setState({username: text})}
            />
          <FormLabel>Password</FormLabel>
          <FormInput
            onChangeText={(text) => this.setState({password: text})}
            />
          <Button
            title='GO'
            onPress={()=> {
              this.props.loginGo(this.state)
              this.props.navigation.navigate('Test', this.props.creds)
            }}
            />
        </View>
      );
    }
  }

const styles = StyleSheet.create({
  inView : {
    marginTop: 50
  }
});

const mapDispatchToProps = dispatch => {
  return {
    loginGo: data => {
      dispatch(login(data))
    }
  }
}

const mapStateToProps = state => {
  return {
    creds: state
  }
}

export default connect(mapStateToProps, mapDispatchToProps)(LogScreen)
