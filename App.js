import React from 'react';
import { StyleSheet, View, Text } from 'react-native';
import { AppLoading, Asset, Font } from 'expo';
import { Button } from 'react-native-elements';
import { RootNavigation } from './navigation/RootNavigation';

import { createStore, applyMiddleware } from 'redux';
import { login } from './src/actions';
import { apiMiddleware, reducer } from './src/reducers';
import { Provider } from 'react-redux';


const initialState = {
  username: '',
  email: '',
  password: ''
}
const store = createStore(reducer, initialState, applyMiddleware(apiMiddleware));


export default class App extends React.Component {

  render() {
    // console.log("*********");
    // store.dispatch(createUser(
    //   {
    //     username: 'aa',
    //     email: 'aa',
    //     password: 'aa'
    //   }
    // ));
    return (
      <Provider store={store}>
        <RootNavigation />
      </Provider>
      );
    }
  }

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#fff',
  },
  inView : {
    marginTop: 200
  }
});
