import express, { Router } from 'express';
import { index, insert, login } from './controllers/users';

const router = Router ();
router.route('/users.json').get(index);
router.route('/signup').post(insert);
router.route('/login').post(login);

export default router;
