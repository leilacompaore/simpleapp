import mongoose from 'mongoose';
import User from './models/user';

const users = [
  {
    username: 'leila',
    email: 'leila@abc.com',
    password: 'pass'
  },
  {
    username: 'jalil',
    email: 'jalil@abc.com',
    password: 'pass'
  }
];

mongoose.connect('mongodb://localhost/users');

users.map( data => {
  const user = new User(data);
  user.save();
});
