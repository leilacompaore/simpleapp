import User from '../models/user';
import moment from 'moment'; //not used

export const index = (req, res, next) => {
  //find all users
  User.find().lean().exec((err, users) => res.json (
    {users}
  ))
};
//ADD a user
export const insert = (req, res, next) => {
  const user = new User(req.body);
  user.save();
  // console.log(req.body);
  res.sendStatus(200);
};

//CHECK PASSWORD FOR LOGGING
export const login = (req, res, next) => {
  User.findOne({username:req.body.username}, 'username email password', function(err, user) {
    if (err) throw err;
    //user does not exist
    if (user === null ) {
      res.sendStatus(404);
    } else {
      //password is good
      if (req.body.password === user.password) {
          // res.sendStatus(200);
          res.json(user)
      } else {
        //password is not good
          res.sendStatus(401);
      }
    }
  })
}
