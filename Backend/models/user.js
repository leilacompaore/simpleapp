import mongoose, { Schema } from 'mongoose';

var userSchema = new Schema ({
  username: String,
  email: String,
  password: String
});
export default mongoose.model('user', userSchema);
