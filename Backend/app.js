import express from 'express';
import mongoose from 'mongoose';
import bodyParser from 'body-parser';
import router from './router';
import morgan from 'morgan';

const app = express();
// app.use(morgan('combined'));
app.use(bodyParser.json());
app.use(bodyParser.urlencoded());


app.use('/api/v1', router);

mongoose.connect('mongodb://localhost/users');



app.get('/', function(req, res) {
  res.send('heyy')
});
// app.get('/v1', function(req, res) {
//   res.send('gh')
// });


app.listen(3000, function() {
  // const { address, port } = app.address();
  console.log('now listening on port 3000')
})
