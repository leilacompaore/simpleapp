import { Notifications } from 'expo';
import React from 'react';
import { StackNavigator } from 'react-navigation';

import ShowScreen from '../screens/ShowScreen';
import SignScreen from '../screens/SignScreen';
import LogScreen from '../screens/LogScreen';
import TestScreen from '../screens/TestScreen';



export const RootNavigation = StackNavigator(
  {
    Main: { screen: ShowScreen },
    Sign: { screen: SignScreen},
    Log: { screen: LogScreen},
    Test: {screen: TestScreen}
  }
);
